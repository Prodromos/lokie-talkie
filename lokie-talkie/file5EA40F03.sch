EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Audio:LM386 U?
U 1 1 5EA41888
P 5600 3100
F 0 "U?" H 5944 3146 50  0000 L CNN
F 1 "LM386" H 5944 3055 50  0000 L CNN
F 2 "" H 5700 3200 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm386.pdf" H 5800 3300 50  0001 C CNN
	1    5600 3100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
